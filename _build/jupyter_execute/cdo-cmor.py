#!/usr/bin/env python
# coding: utf-8

# # cdo cmor: basic standardizing examples ("CMORizing")
# 
# The [CMOR](https://cmor.llnl.gov/) (Climate Model Output Rewriter) software library allows to rewrite
# climate model output in a form that is compliant with the project requirements. This process is often refered to as "CMORizing". 
# 
# CMORizing is necessary for model intercomparison projects (MIPs) so the scientists can benefit from the resulting homogenity in metadata description and data format. Standardized workflows and tools can be applied on the data no matter the source. The alternative would be a big and error prone effort for each individual scientist to process the heterogenous model output of x climate models. The CMORizing process and subsequent quality control is thus an important part of the workflow when submitting data for model intercomparison projects like CMIP6.
# 
# CMOR has been integrated into the popular [CDOs](https://code.mpimet.mpg.de/) (Climate Data Operators) to facilitate the standardization process and to make it more accessible and appealing to a wider community.
# 
# To use the cdo cmor operator, cdo has to be compiled including CMOR. 
# This is not done for the common cdo installations on the levante HPC and also not when installing cdo
# through package managers like APT (eg. `apt-get install cdo`). You can find installation instructions for cdo incl. cmor [here](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_installation).
# On levante, cdo installations that include CMOR can be found under `/work/bm0021/cdo_incl_cmor/`. The latest version is always accessible via `/work/bm0021/cdo_recent_cmor3`.

# In[1]:


# Define alias for the desired cdo cmor installation
get_ipython().run_line_magic('alias', 'cdo "/work/bm0021/cdo_incl_cmor/cdo-2022-09-20_cmor3.6.0_gcc/bin/cdo"')
get_ipython().run_line_magic('store', 'cdo')


# In[2]:


# Necessary imports for loading/plotting the standardized data
import hvplot.xarray, xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs


# ## Project metadata
# 
# The cdo cmor operator can be partly configured via the command line, however also requires input from configuration files containing project, model and operator specific metadata.
# 
# To invoke the cdo cmor operator we need to provide several metadata:
# 
# *  Project metadata / CMOR specific
#     *  **[Controlled vocabulary](https://github.com/WCRP-CMIP/CMIP6_CVs)** ("CV", holding the project metadata including model and experiment definitions as well as part of the projects [DRS (Data Reference Syntax)](https://docs.google.com/document/d/1h0r8RZr_f3-8egBMMh7aqLwy3snpD6_MrDz1q8n5XUk/edit) definition)
#     *  **[MIP tables](https://github.com/PCMDI/cmip6-cmor-tables/tree/master/Tables)** (metadata related to the variables)
# * cdo cmor specific
#   *  **cdocmorinfo** (holding keys that link to the CV [i.e. references to the model or experiment entries of the CV], simulation information and general cdo cmor configuration, can be split over multiple files)
#   *  **mapping table** (holding the information which model variable corresponds to which variable in the MIP tables)

# **Controlled vocabulary**
# 
# The CV can be quite extensive. It also contains a list of required global attributes that are 
# mandatory in every file submitted to the project. Before a file is published via the Earth System Grid Federation ([ESGF](https://esgf.llnl.gov/)), it has to undergo a quality control that performs metadata compliance checks considering both, the CV and the MIP tables. Publication via the DKRZ ESGF node requires additional metadata and data checks. These checks are usually performed by the DKRZ staff.

# In[3]:


mip_table_dir = "/pool/data/CMIP6/cmip6-cmor-tables/Tables/"
CV = mip_table_dir + "CMIP6_CV.json"

get_ipython().system(' head -n 44 {CV}')


# In[4]:


# source-id MPI-ESM1-2-LR
get_ipython().system('grep -A 2 \'"source_id":"MPI-ESM1-2-LR"\' {CV}')


# In[5]:


# experiment historical
get_ipython().system('grep -B 1 \'"experiment_id":"historical"\' {CV}')


# In[6]:


# Amon MIP table - tas entry
get_ipython().system('grep -A 17 \'"tas":\' {mip_table_dir}/CMIP6_Amon.json')


# ## cdo cmor operator

# The cdo cmor operator can be called as follows:
# 
# `cdo cmor,{MIPtable},it={cdocmorinfo_tables_comma_separated},mt={mapping_table},dr={output_path} {input_file}`
# 
# eg. 
# `cdo cmor,Amon,it=cdocmorinfo,exp.cdocmorinfo,model.cdocmorinfo,mt=test_mapping.txt,dr=./ model_data.nc`

# In[7]:


get_ipython().system(' cdo --help cmor')


# **cdocmorinfo**
# 
# Per default, the cdo cmor operator searches for a local and hidden `.cdocmorinfo` file. However, cdocmorinfo files specified in the command line have a higher priority when reading the 
# defined key words.

# In[8]:


# cdocmorinfo (also referred to as infotable)
it = "./config/historical_r1i1p1f1-LR.cdocmorinfo"

get_ipython().system(' cat {it}')


# ## Examples
# 
# In the following 4 examples the CMORization of MPI-ESM1-2-LR model output is demonstrated.

# ### Input files location

# In[9]:


model_data = "/work/bm0021/workshopcmip6pp2022/outdata/"
get_ipython().system(' find {model_data} -type f')


# ### Example 1: GRIB1 input CMORized to CMIP6 standard
# 
# The first example showcases the CMORization of the surface temperature variable. 
# The near surface temperature has the GRIB code 167 in the MPI-ESM echam6 output.
# It also has the variable name tas in the CMIP variable metadata standard, and can be found in various MIP tables. Here we CMORize the CMOR variable tas_Amon.
# The above information is entered in the mapping table:

# In[10]:


# Input file
ifile = model_data + "echam6/historical_r1i1p1f1-LR_echam6_echam_1850.grb"

# Output directory
outdir = "./archive"

# cdocmorinfo - submodel specific
it_echam6 = "./config/cdocmorinfo_echam6_LR"

# mapping table
mt = "./config/mapping_table_example.txt"

# set a fix version date
vdate = "v20221010"


# In[11]:


# Show the content of the mapping table
get_ipython().system(' grep tas {mt}')


# **Run cdo cmor**
# 
# providing the argument `cn=tas` will cause only this variable out of the `Amon` MIP table to be CMORized. If `cn=` is omitted, all variables in `ifile` that are referenced in the mapping table (argument `mt`) will be CMORized and written to the specified output directory (argument `dr`).

# In[12]:


get_ipython().run_line_magic('cdo', 'cmor,Amon,i={it},{it_echam6},mt={mt},dr={outdir},vd={vdate},cn=tas {ifile}')


# **Let us take a look ...**

# In[13]:


# ncdump
outputfile = f"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Amon/"
outputfile += f"tas/gn/{vdate}/tas_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc"
get_ipython().system(' ncdump -h {outputfile}')


# In[14]:


ds = xr.open_dataset(outputfile)

# Plot a single timestep using xarray
ds.tas.isel(time=0).plot();

# Dynamic plot of the entire timeseries using hvplot
#ds.tas.hvplot.quadmesh(width=600)


# ### Example 2: GRIB1 input - diagnostic required
# 
# The next CMOR variable to be created is pr_Amon (precipitation),
# and requires two model variables as in put - large scale precipitation and convective precipitation.
# These two variables have the GRIB codes 142 and 143.
# 
# The operator chaining also works with cdo cmor (with some restrictions that will lead too far for this short introduction), so we can chain an expression to the cdo cmor call:

# **Run cdo cmor**

# In[15]:


get_ipython().run_line_magic('cdo', 'cmor,Amon,i={it},{it_echam6},mt={mt},dr={outdir},vd={vdate},cn=pr -expr,pr=var142+var143 {ifile}')


# **Let us take a look ...**

# In[16]:


# ncdump
outputfile = f"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Amon/"
outputfile += f"pr/gn/{vdate}/pr_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc"
get_ipython().system(' ncdump -h {outputfile}')


# In[17]:


ds = xr.open_dataset(outputfile)

# Plot a single timestep using xarray
ds.pr.isel(time=0).plot();

# Dynamic plot of the entire timeseries using hvplot
#ds.pr.hvplot.quadmesh(width=600)


# ### Example 3: netCDF input
# 
# This example will create the variable tos out of the MPI-ESM1-2 MPIOM variable of the same name. 

# In[18]:


# Input file
ifile = model_data + "mpiom/historical_r1i1p1f1-LR_mpiom_data_2d_mm_18500101_18501231.nc"

# cdocmorinfo - submodel specific attributes
it_mpiom = "./config/cdocmorinfo_mpiom_LR"


# **Let us take a look at the source file**

# In[19]:


get_ipython().system(' ncdump -h {ifile} | head -n 40')


# **Run cdo cmor**

# In[20]:


get_ipython().run_line_magic('cdo', 'cmor,Omon,i={it},{it_mpiom},mt={mt},dr={outdir},vd={vdate},cn=tos {ifile}')


# **Let us take a look ...**

# In[21]:


# ncdump
outputfile = f"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Omon/"
outputfile += f"tos/gn/{vdate}/tos_Omon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc"
get_ipython().system(' ncdump -h {outputfile}')


# In[22]:


ds = xr.open_dataset(outputfile)

# Plot a single timestep using xarray - the curvilinear ocean grid requires some additional attributes
#  for the plot to look proper

fig = plt.figure(figsize=[8, 3])
ax = plt.subplot(1, 1, 1, projection=ccrs.PlateCarree())
ds["tos"].isel(time=0).plot.pcolormesh(ax=ax, x="longitude", y="latitude", 
                                       shading="auto");

# Dynamic plot of the entire timeseries using hvplot
#ds["tos"].hvplot.quadmesh(x="longitude", y="latitude", width=600, projection=ccrs.PlateCarree())


# ### Example 4: netCDF input - 3D data - diagnostic and gridfile required
# 
# The next variable (o2sat on ocean layers) requires a gridfile as input with the specified proper layer midpoints interfaces.
# If no gridfile is provided, cdo cmor will use linear interpolation to infer the layer interfaces
# from the layer midpoints, which is not correct in this case.
# The gridfile can be specified in the cdocmorinfo configuration files.

# In[23]:


# Input file
ifile = model_data + "mpiom/historical_r1i1p1f1-LR_mpiom_data_3d_mm_18500101_18500131.nc"

# cdocmorinfo - submodel specific attributes
it_mpiom = "./config/cdocmorinfo_mpiom_LR"


# Let us also take a look into the mapping table. Since `o2sat` comes with a vertical dimension, 
# we need to specify a `z_axis` for its mapping table entry.

# In[24]:


# Show the content of the mapping table
get_ipython().system(' grep Omon {mt}')


# cdo cmor does not calculate the vertical bounds correctly, which is why provide a `grid_info`:

# In[25]:


# A look into the submodel specific configuration
get_ipython().system(' cat {it_mpiom}')


# This variable requires a quite complex diagnostic, but even this fits into a cdo expression!
# The underscore prefix allows us to define a temporary variable in a cdo expression that is
# not written to the resulting output file. Besides arithmetic operations, the expression operator 
# allows the application of a wide selection of functions, like vertical summation, vertical level
# selection, statistical and conditional operators, etc. 
# (see the [CDO User Guide](https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-3240002.7.1) 
# for more information).

# In[26]:


recipe = "'_TS=ln((298.15-thetao)/(thetao+273.15));o2sat=0.0446596*"
recipe += "exp(2.00907+3.22014*_TS+4.05010*_TS^2+4.94457*_TS^3-0.256847*_TS^4+3.88767*_TS^5"
recipe += "-so*(0.00624523+_TS*0.00737614+0.0103410*_TS^2+0.00817083*_TS^3+0.000000488682*so));'"


# **Run cdo cmor**

# In[27]:


get_ipython().run_line_magic('cdo', 'cmor,Omon,i={it},{it_mpiom},mt={mt},dr={outdir},vd={vdate},cn=o2sat -expr,{recipe} {ifile}')


# **Let us take a look ...**

# In[28]:


# ncdump
outputfile = f"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Omon/"
outputfile += f"o2sat/gn/{vdate}/o2sat_Omon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185001.nc"
get_ipython().system(' ncdump -h {outputfile}')


# In[29]:


ds = xr.open_dataset(outputfile)

# Plot a single timestep using xarray - the curvilinear ocean grid requires some additional attributes
#  for the plot to look proper

fig = plt.figure(figsize=[8, 3])
ax = plt.subplot(1, 1, 1, projection=ccrs.PlateCarree())
ds["o2sat"].isel(time=0, lev=0).plot.pcolormesh(ax=ax, x="longitude", y="latitude", 
                                       shading="auto");

# Dynamic plot of the entire timeseries using hvplot
#ds["o2sat"].isel(time=0).hvplot.quadmesh(x="longitude", y="latitude", width=600, projection=ccrs.PlateCarree())


# ## Minimal Quality Assurance (QA / QC) using [PrePARE](https://cmor.llnl.gov/mydoc_cmip6_validator/)
# 
# PrePARE has been created to check the data with regard to the required attributes (CMIP6 CV and DRS).
# It does not check many aspects of the CF compliance, incomprehensive coordinates etc.
# For this, for example the [AtMoDat Data Checker](https://github.com/AtMoDat/atmodat_data_checker) or the Tool [QA-DKRZ](https://github.com/IS-ENES-Data/QA-DKRZ) can be used.
# 
# Please beware, that you should use PrePARE with the same MIP-tables (i.e. MIP-tables of the same `data_specs_version`) that were used to create the standardized files.

# In[30]:


# PrePARE help message
get_ipython().system('PrePARE -h')


# To check all standardized output at once, we need to provide the `table-path` and the output directory we specified previously for the `cdo cmor` execution via the key-word `dr`.

# In[31]:


get_ipython().system('PrePARE --table-path {mip_table_dir} {outdir}')


# **Example of failing PrePARE check**
# 
# Let's alter the attributes of one of the CMORized files and see PrePARE react:

# In[32]:


get_ipython().run_cell_magic('bash', '-s $outdir', '\ntasfile=$( find ${1} -name "tas_Amon_*.nc" | head -n 1 )\nncatted -O -h -a frequency,global,o,c,"day" ${tasfile}\n')


# In[33]:


get_ipython().system('PrePARE --table-path {mip_table_dir} {outdir}')


# ## Further information and help
# 
# *  Mapping tables for CMIP6 and other projects for models supported by DKRZ: [https://c6dreq.dkrz.de](https://c6dreq.dkrz.de) --> Tab `Variable Mapping`
# *  [CMIP6 DICAD CMORization scripts](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments) for mainly MPI-ESM (access for DKRZ users only)
#    includes [`cdocmorinfo` files / infotables](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments/-/tree/master/namelist_Scenarios/ssp-HR/scripts/it) used in DICAD.
# *  [Create CMIP6 `cdocmorinfo` files / infotables](https://c6dreq.dkrz.de/cdocmorinfo/index.html)
# *  [CMIP6 DICAD CMORizing Tools tutorials](https://c6dreq.dkrz.de/info/workshop_dicad_tools.php)
# *  [cdo cmor documentation incl. handson](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_Operator)
# 
# 

# In[ ]:




