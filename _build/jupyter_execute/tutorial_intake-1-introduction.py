#!/usr/bin/env python
# coding: utf-8

# # Intake I - find, browse and access `intake-esm` collections

# ```{admonition} Overview
# :class: dropdown
# 
# ![Level](https://img.shields.io/badge/Level-Introductory-green.svg)
# 
# 
# 🎯 **objectives**: Learn how to use `intake` to find, browse and access `intake-esm` ESM-collections
# 
# ⌛ **time_estimation**: "30min"
# 
# ☑️ **requirements**: `intake_esm.__version__ >= 2021.8.17`, at least 10GB memory.
# 
# © **contributors**: k204210
# 
# ⚖ **license**:
# 
# ```

# ```{admonition} Agenda
# :class: tip
# 
# In this part, you learn
# 
# 1. [Motivation of intake-esm](#motivation)
# 1. [Features of intake and intake-esm](#features)
# 1. [Browse through catalogs](#browse)
# 1. [Data access via intake-esm](#dataaccess)
#     
# ```

# <a class="anchor" id="motivation"></a>
# 
# We follow here the guidance presented by `intake-esm` on its [repository](https://intake-esm.readthedocs.io/en/latest/user-guide/cmip6-tutorial.html). 
# 
# ## Motivation of intake-esm
# 
# > Simulations of the Earth’s climate and weather generate huge amounts of data. These data are often persisted on different storages in a variety of formats (netCDF, zarr, etc...). Finding, investigating, loading these data assets into compute-ready data containers costs time and effort. The data user needs to know what data sets are available, the attributes describing each data set, before loading a specific data set and analyzing it.
# 
# > `Intake-esm` addresses these issues by providing necessary functionality for **searching, discovering, data access and data loading**.

# For intake users, many data preparation tasks **are no longer necessary**. They do not need to know:
# 
# - 🌍 where data is saved
# - 🪧 how data is saved
# - 📤  how data should be loaded
# 
# but still can search, discover, access and load data of a project.

# <a class="anchor" id="features"></a>
# 
# ## Features of intake and intake-esm
# 
# Intake is a generic **cataloging system** for listing data sources. As a plugin, `intake-esm` is built on top of `intake`, `pandas`, and `xarray` and configures `intake` such that it is able to also **load and process** ESM data.
# 
# - display catalogs as clearly structured tables 📄 inside jupyter notebooks for easy investigation
# - browse 🔍 through the catalog and select your data without
#     - being next to the data (e.g. logged in on dkrz's luv)
#     - knowing the project's data reference syntax i.e. the storage tree hierarchy and path and file name templates
# - open climate data in an analysis ready dictionary of `xarray` datasets 🎁

# All required information for searching, accessing and loading the catalog's data is configured within the catalogs:
# 
# - 🌍 where data is saved
#     * users can browse data without knowing the data storage platform including e.g. the root path of the project and the directory syntax
#     * data of different platforms (cloud or disk) can be combined in one catalog
#     * on mid term, intake catalogs can be **a single point of access**
# - 🪧 how data is saved
#     * users can work with a *xarray* dataset representation of the data no matter whether it is saved in **grb, netcdf or zarr** format.
#     * catalogs can contain more information an therefore more search facets than obvious from names and pathes of the data.
# - 📤  how data should be loaded
#     * users work with an **aggregated** *xarray* dataset representation which merges files/assets perfectly fitted to the project's data model design.
#     * with *xarray* and the underlying *dask* library, data which are **larger than the RAM** can be loaded

# In this tutorial, we load a CMIP6 catalog which contains all data from the pool on DKRZ's mistral disk storage.
# CMIP6 is the 6th phase of the Coupled Model Intercomparison Project and builds the data base used in the IPCC AR6.
# The CMIP6 catalog contains all data that is published or replicated at the ESGF node at DKRZ.

# <a class="anchor" id="terminology"></a>
# 
# ## Terminology: **Catalog**, **Catalog file** and **Collection**
# 
# We align our wording with `intake`'s [*glossary*](https://intake.readthedocs.io/en/latest/glossary.html) which is still evolving. The names overlap with other definitions, making it difficult to keep track. Here we try to give an overview of the hierarchy of catalog terms:
# 
# - a **top level catalog file** 📋 is the **main** catalog of an institution which will be opened first. It contains other project [*catalogs*](#catalog)  📖 📖 📖. Such catalogs can be assigned an [*intake driver*](#intakedriver) which is used to open and load the catalog within the top level catalog file. Technically, a catalog file 📋 is <a class="anchor" id="catalogfile"></a>
#     - is a `.yaml` file
#     - can be opened with `open_catalog`, e.g.:
# ```python
#     intake.open_catalog(["https://dkrz.de/s/intake"])
# ```
# - **intake driver**s also named **plugin**s are specified for [*catalogs*](#catalog) becaues they load specific data sets. There are [many driver](https://intake.readthedocs.io/en/latest/plugin-directory.html) libraries for intake. <a class="anchor" id="intakedriver"></a>.

# - a **catalog** 📖 (or collection) is defined by two parts: <a class="anchor" id="catalog"></a>
#     - a **description** of a group of data sets. It describes how to *load* **assets** of the data set(s) with the specified [driver](#intakedriver). This group forms an entity. E.g., all CMIP6 data sets can be collected in a catalog. <a class="anchor" id="description"></a>
#         - an **asset** is most often a file. <a class="anchor" id="asset"></a>
#     - a **collection** of all [assets](#asset) of the data set(s).   <a class="anchor" id="collection"></a>
#         - the collection can be included in the catalog or separately saved in a **data base** 🗂. In the latter case, the catalog references the data base, e.g.:
# ```json
#   "catalog_file": "/mnt/lustre02/work/ik1017/Catalogs/dkrz_cmip6_disk.csv.gz"
# ```
# 
# ```{note}
# The term *collection* is often used synonymically for [catalog](#catalog).
# ```

# - a *intake-esm* **catalog**  📖 consists of a `.json` file (the **description**) and the underlying data base. The data base is either provided within the `.json` file or as a `.csv.gz` formatted list. 
# 
# The intake-esm catalog can be opened with intake-esm's function `intake.open_esm_datastore()` where the `.json` part is the argument, e.g:
# 
# ```python
# intake.open_esm_datastore("https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/cloud-access/dkrz_cmip6_disk.json")
# ```
#     

# In[1]:


#note that intake_esm is imported with `import intake` as a plugin
import intake


# <a class="anchor" id="browse"></a>
# 
# ## Open and browse through catalogs
# 
# intake (not intake-esm) **opens** catalog-files in `yaml` format. These contain information about additonal sources: other catalogs/collections which will be loaded with specific *plugins*/*drivers*. The command is `open_catalog`.
# 
# <mark> You only need to remember one URL as the *single point of access* for DKRZ's intake catalogs: The DKRZ top level catalog can be accessed via dkrz.de/s/intake . Intake will only follow this *redirect* if a specific parser is activated. This can be done by providing the url in a list.</mark>

# In[2]:


#dkrz_catalog=intake.open_catalog(["https://dkrz.de/s/intake"])
#dkrz_catalog=intake.open_catalog(["/pool/data/Catalogs/dkrz_catalog.yaml"])
#
#only for the web page we need to take the original link:
dkrz_catalog=intake.open_catalog(["https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/cloud-access/dkrz_catalog.yaml"])


# ```{note}
# Right now, two versions of the top level catalog file exist: One for accessing the catalog via [cloud](https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/cloud_access/dkrz_catalog.yaml), one for via [disk](https://gitlab.dkrz.de/data-infrastructure-services/intake-esm/-/raw/master/esm-collections/disk_access/dkrz_catalog.yaml). They however contain **the same content**.
# ```

# We can look into the catalog with `print` and `list`

# Over the time, many collections have been created. `dkrz_catalog` is a **main** catalog prepared to keep an overview of all other collections. `list` shows all sub **project catalogs** which are available at DKRZ.

# In[3]:


list(dkrz_catalog)


# All these catalogs are **intake-esm** catalogs. You can find this information via the `_entries` attribute. The line `plugin: ['esm_datastore']
# ` refers to **intake-esm**'s function `open_esm_datastore()`.

# In[4]:


print(dkrz_catalog._entries)


# The DKRZ ESM-Collections follow a name template:
# 
# `dkrz_${project}_${store}[_${auxiliary_catalog}]`
# 
# where
# 
# - **project** can be one of the *model intercomparison project*, e.g. `cmip6`, `cmip5`, `cordex`, `era5` or `mpi-ge`.
# - **store** is the data store and can be one of:
#     - `disk`: DKRZ holds a lot of data on a consortial disk space on the file system of the High Performance Computer (HPC) where it is accessible for every HPC user. Working next to the data on the file system will be the fastest way possible.
#     - `cloud`: A small subset is transferred into DKRZ's cloud in order to test the performance. swift is DKRZ's cloud storage.
#     - `archive`: A lot of data exists in the band archive of DKRZ. Before it can be accessed, it has to be retrieved. Therefore, catalogs for `hsm` are limited in functionality but still convenient for data browsing.
# - **auxiliary_catalog** can be *grid*

# **Why that convention?**:
# 
# - **dkrz**: Assume you work with internation collections. Than it may become important that you know from where the data comes, e.g. if only pathes on a local file system are given as the locations of the data.
# - **project**: Project's data standards differ from each other so that different catalog attributes are required to identify a single asset in a project data base.
# - **store**: Intake-esm cannot load data from all stores. Before data from the archive can be accessed, it has to be retrieved. Therefore, the opening function is not working for catalog merged for all stores.

# **Best practice for naming catalogs**:
# 
# - Use small letters for all values
# - Do **NOT** use `_` as a separator in values
# - Do not repeat values of other attributes ("dkrz_dkrz-dyamond")

# We could directly start to work with **two intake catalog** at the same time.
# 
# Let's have a look into a master catalog of [Pangeo](https://pangeo.io/):

# In[5]:


pangeo=intake.open_catalog("https://raw.githubusercontent.com/pangeo-data/pangeo-datastore/master/intake-catalogs/master.yaml")


# In[6]:


pangeo 


# In[7]:


list(pangeo)


# While DKRZ's master catalog has one sublevel, Pangeo's is a nested one. We can access another `yaml` catalog which is also a **parent** catalog by simply:

# In[8]:


pangeo.climate


# Pangeo's ESM collections are one level deeper in the catalog tree:

# In[9]:


list(pangeo.climate)


# ### The `intake-esm` catalogs
# 
# We now look into a catalog which is opened by the plugin `intake-esm`. 
# 
# > An ESM (Earth System Model) collection file is a `JSON` file that conforms to the ESM Collection Specification. When provided a link/path to an esm collection file, intake-esm establishes a link to a database (`CSV` file) that contains data assets locations and associated metadata (i.e., which experiment, model, the come from).
# 
# Since the data base of the CMIP6 ESM Collection is about 100MB in compressed format, it takes up to a minute to load the catalog.

# ```{note}
# The project catalogs contain only valid and current project data. They are constantly updated.
# 
# If your work is based on a catalog and a subset of the data from it, be sure to save that subset so you can later compare your database to the most current catalog.
# ```

# In[10]:


esm_col=dkrz_catalog.dkrz_cmip6_disk
print(esm_col)


# `intake-esm` gives us an overview over the content of the ESM collection. The ESM collection is a data base described by specific attributes which are technically columns. Each project data standard is the basis for the columns and used to parse information given by the path and file names.
# 
# The pure display of `esm_col` shows us the number of unique values in each column. Since each `uri` refers to one file, we can conclude that the DKRZ-CMIP6 ESM Collection contains **6.1 Mio Files** in 2022.

# The data base is loaded into an underlying `panda`s dataframe which we can access with `esm_col.df`. `esm_col.df.head()` displays the first rows of the table:

# In[11]:


esm_col.df.head() 


# We can find out details about `esm_col` with the object's attributes. `esm_col.esmcol_data` contains all information given in the `JSON` file. We can also focus on some specific attributes.

# In[12]:


#esm_col.esmcol_data


# In[13]:


print("What is this catalog about? \n" + esm_col.esmcol_data["description"])
#
print("The link to the data base: "+ esm_col.esmcol_data["catalog_file"])


# Advanced: To find out how many datasets are available, we can use pandas functions (drop columns that are irrelevant for a dataset, drop the duplicates, keep one):

# In[14]:


cat = esm_col.df.drop(['uri','time_range'],1).drop_duplicates(keep="first")
print(len(cat))


# ### Browse through the data of the ESM collection

# You will browse the collection technically by setting values the **column names** of the underlying table. Per default, the catalog was loaded with all cmip6 attributes/columns that define the CMIP6 data standard:

# In[15]:


esm_col.df.columns


# These are configured in the top level catalog so you <mark> do not need to open the catalog to see the columns </mark>

# In[16]:


dkrz_catalog._entries["dkrz_cmip6_disk"]._open_args


# Most of the time, we want to set more than one attribute for a search. Therefore, we define a query `dict`ionary and use the `search` function of the `esm_col` object. In the following case, we look for temperature at surface in monthly resolution for 3 different experiments:

# In[17]:


query = dict(
    variable_id="tas",
    table_id="Amon",
    experiment_id=["piControl", "historical", "ssp370"])
# piControl = pre-industrial control, simulation to represent a stable climate from 1850 for >100 years.
# historical = historical Simulation, 1850-2014
# ssp370 = Shared Socioeconomic Pathways (SSPs) are scenarios of projected socioeconomic global changes. Simulation covers 2015-2100
cat = esm_col.search(**query)


# In[18]:


cat


# We could also use *Wildcards*. For example, in order to find out which ESMs of the institution *MPI-M* have produced data for our subset:

# In[19]:


cat.search(source_id="MPI-ES*")


# We can find out which models have submitted data for at least one of them by:

# In[20]:


cat.unique(["source_id"])


# If we instead look for the models that have submitted data for ALL experiments, we use the `require_all_on` keyword argument:

# In[21]:


cat = esm_col.search(require_all_on=["source_id"], **query)
cat.unique(["source_id"])


# Note that only the combination of a `variable_id` and a `table_id` is unique in CMIP6. If you search for `tas` in all tables, you will find many entries more:

# In[22]:


query = dict(
    variable_id="tas",
#    table_id="Amon",
    experiment_id=["piControl", "historical", "ssp370"])
cat = esm_col.search(**query)
cat.unique(["table_id"])


# Be careful when you search for specific time slices. Each frequency is connected with a individual name template for the filename. If the data is yearly, you have YYYY-YYYY whereas you have YYYYMM-YYYYMM for monthly data. 

# ### How to load more columns
# 
# Intake allows to load only a subset of the columns that is inside the **intake-esm** catalog. Since the memory usage of **intake-esm** is high, the default columns are only a subset from all possible columns. Sometimes, other columns are of interest:
# 
# If you work remotely away from the data, you can use the **opendap_url**'s to access the subset of interest for all files published at DKRZ. The *opendap_url* is an *additional* column that can also be loaded.
# 
# We can define 3 different column name types for the usage of intake catalogs:
# 
# 1. **Default** attributes which are loaded from the main catalog and which can be seen via `_entries[CATNAME]._open_args`.
# 2. **Overall** attributes or **template** attributes which should be defined for **ALL** catalogs at DKRZ (exceptions excluded). At DKRZ, we use the newly defined **Cataloonie** scheme template which can be found via `dkrz_catalog.metadata["parameters"]["cataloonie_columns"]`. With these template attributes, there may be redundancy in the columns. They exist to simplify merging catalogs across projects.
# 3. **Additional** attributes which are not necessary to identify a single asset but helpful for users. You can find these via
# 
# `dkrz_catalog.metadata["parameters"]["additional_PROJECT_columns"]`
# 
# So, for CMIP6 there are:

# In[23]:


dkrz_catalog.metadata["parameters"]["additional_cmip6_disk_columns"]


# ```{tip}
# You may find *variable_id*s in the catalog which are not obvious or abbrevations for a clear variable name. In that cases you would need additional information like a *long_name* of the variable. For CMIP6, we provided the catalog with this `long_name` so you could add it as a column.
# ```

# So, this is the instruction how to open the catalog with additional columns:
# 
# 1. create a combination of all your required columns:

# In[24]:


cols=dkrz_catalog._entries["dkrz_cmip6_disk"]._open_args["csv_kwargs"]["usecols"]+["opendap_url"]


# 2. open the **dkrz_cmip6_disk** catalog with the `csv_kwargs` keyword argument in this way:

# In[25]:


esm_col=dkrz_catalog.dkrz_cmip6_disk(csv_kwargs=dict(usecols=cols))


# - ⭐ The customization of catalog columns allows highest flexibility for intake users. 
# - ⭐ In theory, we could add many more columns with additional information because ot all have to be loaded from the data base.

# ```{warning}
# The number of columns determines the required memory.
# ```

# ```{tip}
# If you work from remote and also want to access the data remotely, load the *opendap_url* column.
# ```

# <a class="anchor" id="dataaccess"></a>
# 
# ## Access and load data of the ESM collection
# 
# With the power of `xarray`, `intake` can load your subset into a `dict`ionary of datasets. We therefore focus on the data of `MPI-ESM1-2-LR`:

# In[26]:


#case insensitive?
query = dict(
    variable_id="tas",
    table_id="Amon",
    source_id="MPI-ESM1-2-LR",
    experiment_id="historical")
cat = esm_col.search(**query)
cat


# You can find out which column intake uses to access the data via the following keyword:

# In[27]:


print(cat.path_column_name)


# As we are working with the *_disk* catalog, **uri** contains *pathes* to the files on filesystem. If you are working from remote, you would have
# 
# - to change the catalog's attribute `path_column_name` to *opendap_url*.
# - to reassign the `format` column from *netcdf* to *opendap*
# 
# as follows:

# In[28]:


#cat.path_column_name="opendap_url"
#newdf=cat.df.copy()
#newdf.loc[:,"format"]="opendap"
#cat.df=newdf


# **Intake-ESM** natively supports the following data formats or access formats (since opendap is not really a file format):
# 
# - netcdf
# - opendap
# - zarr
# 
# You can also open **grb** data but right now only by specifying xarray's attribute *engine* in the *open* function which is defined in the following. I.e., it does not make a difference if you specify **grb** as format.
# 
# You can find an example in the *era5* notebook.

# The function to open data is `to_dataset_dict`. 
# 
# We recommend to set a keyword argument `cdf_kwargs` for the chunk size of the variable's data array. Otherwise, `xarray` may choose too large chunks. Most often, your data contains a time dimension so that you could set `cdf_kwargs={"chunks":{"time":1}}`. 
# 
# If your collection contains **zarr** formatted data, you need to add another keyword argument `zarr_kwargs`. <mark> The trick is: You can just specify both. Intake knows from the `format` column which *kwargs* should be taken.

# In[29]:


xr_dict = cat.to_dataset_dict(cdf_kwargs=dict(chunks=dict(time=1)),
                             zarr_kwargs=dict(consolidated=True)
                                              #decode_times=True,
                                              #use_cftime=True)
                             )
xr_dict


# `Intake` was able to aggregate many files into only one dataset:
# - The `time_range` column was used to **concat** data along the `time` dimension
# - The `member_id` column was used to generate a new dimension
# 
# The underlying `dask` package will only load the data into memory if needed. Note that attributes which disagree from file to file, e.g. *tracking_id*, are excluded from the dataset.

# How **intake-esm** should open and aggregate the assets is configured in the *aggregation_control* part of the description:

# In[30]:


print(esm_col.esmcol_data["aggregation_control"]["aggregations"])


# Columns can be defined for appending or creating new dimensions. The *options* are keyword arguments for xarray.
# 
# They **keys** of the dictionary are made with column values defined in the *aggregation_control* of the **intake-esm** catalog. These will determine the **key_template**. The corresponding commands are:

# In[31]:


print(cat.esmcol_data["aggregation_control"]["groupby_attrs"])
#
print(cat.key_template)


# You can work with these keys **directly** on the **intake-esm** catalog which will give you an overview over all columns (too long for the web page):

# In[32]:


#cat["CMIP.MPI-ESM1-2-HR.historical.Amon.gn"]


# If we are only interested in the **first** dataset of the dictionary, we can *pop it out*:

# In[33]:


xr_dset = xr_dict.popitem()[1]
xr_dset


# ### Troubleshooting
# 
# The variables are collected in **one** dataset. This requires that **the dimensions and coordinates must be the same over all files**. Otherwise, xarray cannot merge these together.
# 
# For CMIP6, most of the variables collected in one **table_id** should be on the same dimensions and coordinates. Unfortunately, there are exceptions.: 
# 
# - a few variables are requested for *time slices* only. 
# - sometimes models use different dimension names from file to file
# 
# Using the [preprocessing](https://tutorials.dkrz.de/tutorial_intake-4-preprocessing-derived-vars.html#use-preprocessing-when-opening-assets-and-creating-datasets) keyword argument can help to rename dimensions before merging.
# 
# For Intake providers: the more information on the dimensions and coordinates provided already in the catalog, the better the aggregation control.

# ### Pangeo's data store
# 
# Let's have a look into Pangeo's ESM Collection as well. This is accessible via cloud from everywhere - you only need internet to load data. We use the same `query` as in the example before.

# In[34]:


pangeo_cmip6=pangeo.climate.cmip6_gcs
cat = pangeo_cmip6.search(**query)
cat


# There are differences between the collections because
# - Pangeo provides files in *consolidated*, `zarr` formatted datasets which correspond to `zstore` entries in the catalog instead of `path`s or `opendap_url`s. 
# - The `zarr` datasets are already aggregated over time so there is no need for a `time_range` column
# 
# If we now open the data with `intake`, we have to specify keyword arguments as follows:

# In[35]:


dset_dict = cat.to_dataset_dict(
    zarr_kwargs={"consolidated": True}#, "decode_times": True, "use_cftime": True}
)


# In[36]:


dset_dict


# `dset_dict` and `xr_dict` are the same. You succesfully did the intake tutorial!

# ### Making a quick plot
# 
# The following line exemplifies the ease of the intake's data processing library chain. On the web page, the interactivity will not work as all plots would have to be loaded which is not feasible.
# 
# For more examples, check out the **use cases** on that web page.

# In[37]:


import hvplot.xarray
dset_dict.popitem()[1]["tas"].hvplot.quadmesh(width=600)


# ```{seealso}
# This tutorial is part of a series on `intake`:
# * [Part 1: Introduction](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-1-introduction.html)
# * [Part 2: Modifying and subsetting catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-2-subset-catalogs.html)
# * [Part 3: Merging catalogs](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-3-merge-catalogs.html)
# * [Part 4: Use preprocessing and create derived variables](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-4-preprocessing-derived-variables.html)
# * [Part 5: How to create an intake catalog](https://data-infrastructure-services.gitlab-pages.dkrz.de/tutorials-and-use-cases/tutorial_intake-5-create-esm-collection.html)
# 
# - You can also do another [CMIP6 tutorial](https://intake-esm.readthedocs.io/en/latest/user-guide/cmip6-tutorial.html) from the official intake page.
# 
# ```

# 
# 
