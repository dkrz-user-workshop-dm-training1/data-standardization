#!/usr/bin/env python
# coding: utf-8

# ***
# # Demo Jupyter Notebook for the DKRZ User Workshop ![DKRZ Logo](https://www.dkrz.de/@@site-logo/dkrz.svg)
# # Data Standardization: Simple Tools for Achieving FAIR CF-compliant Data
# _Angelika Heil (heil@dkrz.de), 13 October 2022_
# ***

# <div class="alert-info" role="alert">
#   <h4 class="alert-heading"><span style="font-family: Arial; font-size:1.4em;color:black;"> README</h4>
#  
#    In this Jupyter Notebook with a IPython kernel, we use both, python and BASH commands.  <br>  
#    To execute BASH commands, put  <br> 
#                      (i)   %%bash is in first line of a cell --> entire cell is BASH  <br> 
#                      (ii)  !command                          --> individual line is BASH. <br> 
#                      (iii) {pyvar} to pass a python variable to BASH<br> </p>
#  <p><span style="font-family: Arial; font-size:1.0em;color:black;"> Please also read our theoretical introduction on data standardization on https://gitlab.dkrz.de/dkrz-user-workshop-dm-training1/data-standardization (see Presentation Slides)        <br>   </p> 
#   <hr>
# </div>

# ## Step 0: Load the required libraries

# In[1]:


import numpy as np
import xarray as xr
import cf_units
import glob, os
from datetime import datetime
import pandas as pd


# In[2]:


#-- Customize cell output
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"


# In[3]:


# Create working directory
# In case of preexisting working directory, remove all netCDF files in that directory
odir = "wd22_ds"
try:
    os.mkdir(odir)
except FileExistsError:
    print("directory", odir , "already existed! Removing netCDF files...")
    _ = [os.remove(x) for x in glob.glob(odir+"/*.nc")]


# ## Step 1: Create a netCDF file with some dummy 3D data
# This could be some 2m air temperature model output over South Africa

# In[4]:


#-- Create data as numpy arrays
nlat, nlon =  2, 4
lat, lon   =  np.linspace(-34, -22, nlat), np.linspace(16, 32, nlon)      #-- roughly domain of South Africa
ntime      =  3                                                           #-- number of time steps
time       =  np.linspace(0.5, 0.5 + ntime, num = ntime, endpoint=False)  #-- "mid-point" time coordinate values
tas        =  5 + 10 * np.random.randn(ntime, nlat, nlon)                 #-- some fake 2m air temperature data


# In[5]:


#-- Store data as xarray dataarray
da         = xr.DataArray(data=tas, dims=["t", "lat", "lon"], coords=[time, lat, lon])


# In[6]:


#-- Write out as netCDF
da.rename("tas").to_netcdf(f"{odir}/unstandardized.nc", mode="w", format="NETCDF3_64BIT", 
                            encoding={"lat":{"_FillValue": None}, 
                                      "lon":{"_FillValue": None}, 
                                      "t":{"_FillValue": None},
                                      "tas":{"_FillValue": -999.9}})          
#-- Encoding _FillValue with None suppresses that xarray creates a _FillValue attribute for lat, lon, t. 
#-- _FillValue attributes along with coordinate variables are not CF-compliant.
#-- For tas, we define a _FillValue of -999.9 to represent missing data.


# ## Step 2: Looking at the unstandardized netCDF file

# ### (2a) Look at unstandardized netCDF file with NCO and CDO

# In[7]:


print("\n", "=" * 10, " unstandardized netCDF dummy file: the netCDF header   ", "=" * 10)
#-- -h option means that only the so-called "netCDF header" is shown, and not the data
get_ipython().system('ncdump -h {odir}/unstandardized.nc')

print("\n", "=" * 10, " unstandardized netCDF dummy file: CDO short info      ", "=" * 10)
#--  cdo sinfo gives short overview of how the file is structured
get_ipython().system('cdo -s sinfo {odir}/unstandardized.nc')


# <span style="color:red">This unstandardized file has no metadata describing the data.<br>
#     It remains unclear what the variable _var_ represents, and what the dimensions and the units are.<br>
#     Also note: CDO interprets _t_ as vertical coordinates while it was meant to be time.</span>

# ### (2b) Check CF compliance

# **Run the CF checker _cfchecks_ on the unstandardized netCDF file**

# In[8]:


get_ipython().system('cfchecks {odir}/unstandardized.nc')


# <span style="color:red">In a completely unstandardized netCDF file, the CF checker tool output is misleading.<br> 
#     0 ERRORS detected does not mean that the file conforms with the CF conventions. Please carefully look at the WARNINGS!<br> 
#     The global attribute _Conventions_ must be specified in a netCDF-CF header.<br>
#     Principally, if a standard name exists for a variable, then the standard name and its unit should be added as variable attribute.<br>
#     If no CF standard name exists, then it is highly recommended to add a unit and a user-defined _long_name_.<br>
# </span>

# ### (2c) Plot the data in the unstandardized netCDF file with xarray

# **Open the data and plot an overview of the file content**

# In[9]:


da = xr.open_dataset(f"{odir}/unstandardized.nc")
da


# **Plot the data (first "timestep" t)**

# In[10]:


da.tas.isel(t=0).plot();


# <span style="color:red">From an unstandardized file, insufficient labels are added to the plot... </span>

# ## Step 3: Let's create a netCDF file with a minimalistic standardization

# ### (3a) Define some minimal metadata

# **Where to find out the correct metadata to describe the variables?** <br>
# It is best practice to add a CF standard name as attribute to each variable. <br>
# A list of all CF standard names is provided at https://cfconventions.org/Data/cf-standard-names/current/build/cf-standard-name-table.html<br>
# If there is no standard name for your variable, add a user-defined long_name attribute instead.

# ***
# ***
#   ***How to work with the "canonical units" specified in the CF standard name table?*** <br>
#   *–––––––––––––––––––––––&emsp;&emsp; Example  &emsp;&emsp;    –––––––––––––––––––––––* <br>
# 
# **(A) How to find out Udunits2-compatible units for *K*?** <br>
# * The CF standard name of the tas variable is *air_temperature*; the corresponding canonical units is *K* (i.e. Kelvin).
# * Canonical units means that you can also use another units, provided that it is Udunits2-compatible.
# * Udunits2 is the Unidata units library (https://github.com/Unidata/UDUNITS-2). It is included in the _cf_units_ library imported in Step 0.

# In[11]:


#-- We first check in general if "degC" is a valid Udunits2 units
tarun="degC"          #-- target unit
print("Is \""+tarun+"\" a valid Udunits2 unit? ",  cf_units.Unit(tarun).is_udunits())
#tarun="°c"; #print("Is "+tarun+" a valid Udunits2 unit? ",  cf_units.Unit(tarun).is_udunits()) #==> will raise a ValueError


# In[12]:


#-- We then check if "degC" is Udunits2-compatible to the canonical units K
tarun="degC"          #-- target units
canun="K"             #-- canonical units
print("Is \""+tarun+"\" a Udunits2-compatible units to \""+canun+"\"? ",  cf_units.Unit(canun).is_convertible(cf_units.Unit(tarun)))
#--Non-compatible units will raise a ValueError


# **(B) How to find out CF-compliant units for the *time* coordinate variable?** <br>
# * The CF standard name of the time coordinate variable is *time*; the corresponding canonical units is *s* (i.e. seconds).<br>
# * We want the corresponding units to be _days since 2000-01-01_, i.e. a relative time axis with days as increment.<br>
#  ***this is a bit special CF feature, but still easy to check:***
#   

# In[13]:


#-- We first check 
tarun="days";canun="s"
print("Is \'"+tarun+"\' a Udunits2-compatible units to \'"+canun+"\'? ",  cf_units.Unit(canun).is_convertible(cf_units.Unit(tarun)))
#-- Alternatively, we could check if "days" is a Udunits2-compatible units for time:
print("Is \'"+tarun+"\' a valid Udunits2 time unit? ", cf_units.Unit(tarun).is_time())
#-- What about "days since 2000-01-01"? ..we have to check if this is a Udunits2 valid reference time....
print("Is \'"+tarun+" since 2000-01-01' a a valid Udunits2 time reference? ",
      cf_units.Unit(tarun+" since 2000-01-01").is_time_reference())


# In[14]:


#-- With a relative time axis, the calendar should be specified. 
print("What are permissible entries for the time attribute 'calendar'?: \n", cf_units.CALENDARS)
#-- A _standard_ calendar is appropriate for the dummy data.


# ***

# **Define minimal metadata**

# In[15]:


#-- (i)  CF mandatory global attribute Conventions = "CF-X.X"
glo_Con = "CF-1.8"                #-- here we choose CF-1.8 which is the latest version supported 
                                  #   by the CF compliance checker 
#-- (ii) Variable attributes
lat_stn = "latitude"              #-- CF standard name of latitude coordinate variable
lat_uni = "degrees_north"         #-- Units compatible with the CF standard name latitude
lon_stn = "longitude"
lon_uni = "degrees_east"
tim_stn = "time"
tim_uni = "days since 2000-01-01"
tim_cal = "standard"
tas_stn = "air_temperature"
tas_uni = "degC"


# ### (3b) Add the metadata with NCO

# Note: _In the default Levante environment, it would be necessary to load the NCO module with `module load nco`_

# In[16]:


#-- Rename t into time with NCO rename variable/dimension (-v/-d) old,new.
#-- This is just nice to have so that CDO correctly inteprets time dimension.
get_ipython().system('ncrename -O -v t,time -d t,time {odir}/unstandardized.nc {odir}/tmp.nc')
#-- Note: due to some NCO bug https://github.com/Unidata/netcdf-c/issues/597, 
#   this only works with netCDF3, and not netCDF4.
#  After renaming, we convert the file to netCDF4 classic:
get_ipython().system('ncks --fl_fmt=netcdf4_classic {odir}/tmp.nc {odir}/mininal_standardized_nco.nc; rm  {odir}/tmp.nc')

#-- Add attributes with NCO's ncatted, passing python variable to BASH with {pyvar}   
#-- (i) Add global attributes
get_ipython().system('ncatted -O -a Conventions,global,c,c,"{glo_Con}" {odir}/mininal_standardized_nco.nc')
#-- (ii) Add attributes to the variables
get_ipython().system('ncatted -O  -a standard_name,lat,c,c,"{lat_stn}" -a units,lat,c,c,"{lat_uni}"               -a standard_name,lon,c,c,"{lon_stn}" -a units,lon,c,c,"{lon_uni}"               -a standard_name,time,c,c,"{tim_stn}" -a units,time,c,c,"{tim_uni}"               -a calendar,time,c,c,"{tim_cal}"               -a standard_name,tas,c,c,"{tas_stn}" -a units,tas,c,c,"{tas_uni}"               {odir}/mininal_standardized_nco.nc')


# ### (3c) Standardize data with xarray

# **This python code does the same as the NCO commands above**<br>

# In[17]:


ds = xr.open_dataset(f"{odir}/unstandardized.nc")
ds_min = ds.rename({"t": "time"})


# In[18]:


ds_min.attrs['Conventions'] = glo_Con

ds_min["lat"].attrs  = { "standard_name": lat_stn, "units": lat_uni } 
ds_min["lon"].attrs  = { "standard_name": lon_stn, "units": lon_uni }
ds_min["time"].attrs = { "standard_name": tim_stn, "units": tim_uni, "calendar": tim_cal }
ds_min["tas"].attrs  = { "standard_name": tas_stn, "units": tas_uni }


# In[19]:


ds_min.to_netcdf(f"{odir}/mininal_standardized_xr.nc",
                 mode="w", 
                 format="NETCDF4_CLASSIC", 
                 encoding=
                 {
                     "lat":{"_FillValue": None},
                     "lon":{"_FillValue": None},
                     "time":{"_FillValue": None},
                 })  


# ## Step 4: Now let's look at the minimally standardized netCDF file

# ### (4a) Look at the netCDF file with NCO and CDO

# In[20]:


print("\n", "=" * 10, " minimally standardized netCDF dummy file: the netCDF header ", "=" * 10)
get_ipython().system('ncdump -h {odir}/mininal_standardized_nco.nc')

print("\n", "=" * 10, " minimally standardized netCDF dummy file: CDO short info ", "=" * 10)
get_ipython().system('cdo -s sinfo {odir}/mininal_standardized_nco.nc')


# ### (4b) Check CF compliance

# **Run the CF checker _cfchecks_ on the minimally standardized netCDF files**

# In[21]:


print("\n", "=" * 10,
      "    NCO minimally standardized netCDF dummy file: CF checker summary    ",
      "=" * 10
     )
get_ipython().system('cfchecks {odir}/mininal_standardized_nco.nc|tail -3')

print("\n", "=" * 10,
      " xarray minimally standardized netCDF dummy file: CF checker summary    ",
      "=" * 10
     )
get_ipython().system('cfchecks {odir}/mininal_standardized_xr.nc|tail -3')


# <span style="color:red">The CF checker has nothing to complain about the minimally standardized files (created with either NCO or xarray) </span>

# ### (4c) Plot the data in the minimally standardized netCDF file with xarray

# In[22]:


da  = xr.open_dataset(f"{odir}/mininal_standardized_nco.nc")
print("\n", "=" * 10, " xarray single plot    ", "=" * 10)
fig = da.tas.isel(time=0).plot();


# In[23]:


print("\n", "=" * 10, " xarray multipanel plot    ", "=" * 10)
fig = da.tas.plot(x="lon", y="lat", col="time", col_wrap=3); #-- make a multipanel plot with 3 columns

#-- In faceted plots, xarray only prints the variable names as x and y axis labels. 
#-- You can use the standard_name and units attribute as follows: 
fig.set_axis_labels(da.lon.standard_name+" ["+da.lon.units+"]");   #-- adds an xlabel to each subplot
#-- If you prefer a single central xlabel to all subplots; please uncomment: 
#for i, ax in enumerate(fig.axes.flat):
#    #-- remove the xlabels
#    ax.set_xlabel("");  
#fig.axes.flat[1].set(xlabel=da.lon.standard_name+" ["+da.lon.units+"]");


# 
# ## <br>Step 5: Let's "pimp" the netCDF file towards the ATMODAT Standard

# _For more on the ATMODAT Standard, please see: <br>
# Ganske, Anette; Kraft, Angelina; Kaiser, Amandine; Heydebreck, Daniel; Lammert, Andrea; Höck, Heinke; Thiemann, Hannes; Voss, Vivien; Grawe, David; Leitl, Bernd; Schlünzen, K. Heinke; Kretzschmar, Jan; Quaas, Johannes (2021). ATMODAT Standard (v3.0). World Data Center for Climate (WDCC) at DKRZ. https://doi.org/10.35095/WDCC/atmodat_standard_en_v3_0_

# **Define some essential ATMODAT global metadata**

# In[24]:


#-- Complement the global Conventions attribute with ATMODAT standard.
add2Conventions="ATMODAT-3.0"

#-- A meaningful title describing the data, similar to the title in a paper publication.
title="Fake model data of 2m air temperature over South Africa"

#-- The name of the institution principally responsible for originating this data.
institution="German Climate Computing Center, Hamburg, Germany"

#-- If available, institute's ROR identifier, see https://ror.org/.
institution_id="https://ror.org/03ztgj037"

#-- Contact person or point and contact details (free form).
#-- This does not need to be the creator of the data, but rather contact details that are valid in long-term.
#-- For persons, it is recommended to add e.g. their ORCID, see https://orcid.org/
contact="Data Management, data@dkrz.de, German Climate Computing Center, Hamburg, Germany"

#-- Person who created the dataset. The full name and ORCID should be included.
creator="Angelika Heil, heil@dkrz.de, https://orcid.org/0000-0002-8768-5027"

#-- Provide the name of a commonly known license (see also SPDX list of licenses: https://spdx.org/licenses/).
#-- It is recommended to use the SPDX short identifier.
#-- CC-BY-4.0 is the most commonly used license for research data publications.
license="CC-BY-4.0"

#-- Provide details on how the data were produced. In case of model data, mention the model components.
source="Fake data created with numpy random function"

#-- Provide a summary; for details read this summary text.
summary="Here comes a paragraph describing the dataset, analogous to an abstract for a paper. \
It is recommended to include citations (e.g. to literature describing how the data were produced \
or how they were evaluated). \
Also links to websites can be included. The reference list should be included in the \
references global attribute. \
Example: Using sea level pressure distributions from the NCEP/NCAR Reanalysis 1 \
(Kalnay et al., 1996), downloaded from \
https://psl.noaa.gov/data/gridded/data.ncep.reanalysis.html \
(last accessed October 7, 2022), ......"

#-- Provide a reference list.
references="Kalnay et al. (1996), https://doi.org/10.1175/1520-0477(1996)077<0437:TNYRP>2.0.CO;2\
; XY et al. (YYYY)....."

#-- It is recommended to assign a version identiﬁer to a data product.
product_version="v0"

#-- Add a creation date in YYYY-MM-DDThh:mm:ss<zone> ISO 8601:2004 extended format.
creation_date=datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + "Z"

#-- Add an audit trail (history) for modifications to the original data.
#-- NCO and CDO automatically appends history entries, while xarray does not. 
#-- In the latter case, it would be optimal to manually add a summary of the xarray operations
#-- applied upon the data. However, it already helps to include the name of the script used.
add2history=creation_date + ": Data standardization with Simple_Data_Standardization.ipynb"


#-- Add some keywords describing your data, using commata to separate them.
#-- This is particularly helpful for the findability of your data.
#-- Data repositories frequently display the netCDF file headers and search machines will look in them for keywords.
#-- Please use keywords that belong to an existing controlled vocabulary. 
#-- For Earth Sciences, the GCMD Science Keyword Compendium is a good place to look for (see https://gcmd.earthdata.nasa.gov/KeywordViewer/).
#-- Please note that it is common practice to provide the full path of a GCMD keyword (see example).
#-- You can also convert the CF standard_name into a GCMD keywords: https://erddap.ifremer.fr/erddap/convert/keywords.html.
#-- If your data have a spatial reference (e.g. Baltic Region or Indian Ocean), please look if there is a corresponding keyword in https://www.geonames.org/. 
keywords="Earth Science > Atmosphere > Atmospheric Temperature > Surface Air Temperature, South Africa, fake data"

#-- Add the name of the keyword compendium from which the keywords originate. Specify None if the keyword is not from a controlled vocabulary.
keywords_vocabulary="GCMD, geonames, None" 


#-- Add some comment that might be helpful to better use the data
comment="The data were created in the framework of the 2022 DKRZ user workshop. They are solely for test purposes."

#-- In case you intend to publish your data via the WDCC, please add the following global attributes.
#publisher_name="World Data Center for Climate (WDCC) at DKRZ"
#publisher_url="https://www.wdc-climate.de"
#-- The website will be created by the WDCC curators
#metadata_link="https://www.wdc-climate.de/ui/entry?acronym=YYYYYYYYYYY"


# ### (5a) Improve the FAIRness using NCO

# **Let's add the global metadata with NCO**

# In[25]:


get_ipython().system('ncatted -O -a Conventions,global,a,c," {add2Conventions}"              -a history,global,a,c,"\\n {add2history}"              -a title,global,c,c,"{title}"              -a institution,global,c,c,"{institution}"              -a institution_id,global,c,c,"{institution_id}"              -a creator,global,c,c,"{creator}"              -a contact,global,c,c,"{contact}"              -a license,global,c,c,"{license}"              -a source,global,c,c,"{source}"              -a summary,global,c,c,"{summary}"              -a references,global,c,c,"{references}"              -a product_version,global,c,c,"{product_version}"              -a creation_date,global,c,c,"{creation_date}"              -a keywords,global,c,c,"{keywords}"              -a keywords_vocabulary,global,c,c,"{keywords_vocabulary}"              -a comment,global,c,c,"{comment}"      {odir}/mininal_standardized_nco.nc {odir}/ATMODAT_standardized_nco.nc')


# **Add some "Nice-to-haves" for more FAIRness with NCO** <br>
# * Add auxillary coordinate variable to specify for which height the variable tas is representative
# * Add bounds to the time coordinate variable

# In[26]:


#-- In our case, we want the 2m air temperature.
#-- The height:positive = "up" is to avoid that the CDOs raise a warning
get_ipython().system('ncap2 -o {odir}/ATMODAT_standardized_nco.nc -O -s "height=2.0" {odir}/ATMODAT_standardized_nco.nc')
get_ipython().system('ncap2 -o {odir}/ATMODAT_standardized_nco.nc -O -s \'defdim("nv",2);time_bnds=make_bounds(time,$nv);\'               {odir}/ATMODAT_standardized_nco.nc')
#-- Note that the NCO function make_bounds adds attributes to the time_bnds variable which is not recommended by CF
#-- We remove all attributes to the time_bnds variable with -a ,time_bnds,d,,
get_ipython().system('ncatted -O -a standard_name,height,c,c,"height" -a units,height,c,c,"m" -a positive,height,c,c,"up"              -a coordinates,tas,c,c,"height lat lon" -a cell_methods,tas,c,c,"time: maximum"              -a bounds,time,c,c,"time_bnds" -a ,time_bnds,d,,  {odir}/ATMODAT_standardized_nco.nc')


# ### (5b) Improve the FAIRness using xarray

# In[27]:


ds     = xr.open_dataset(f"{odir}/mininal_standardized_xr.nc")
ds     = ds.assign_coords(height=2)
ds["height"].attrs = {"standard_name": "height", "units": "m", "positive": "up"}

ds.attrs["Conventions"] += " " + add2Conventions
ds.attrs["history"] = add2history
#-- this appends additional global attributes
ds.attrs.update({
                "title": title, 
                "institution": institution,
                "institution_id": institution_id,
                "creator": creator,
                "contact": contact,
                "license": license,
                "source": source,
                "summary": summary,
                "references": references,
                "product_version": product_version,
                "creation_date": creation_date,
                "keywords": keywords,
                "keywords_vocabulary": keywords_vocabulary,
                "comment": comment,
                })

#-- Create and append time bounds
timebnds_s, timebnds_e = ds.time - pd.Timedelta(days=0.5), ds.time + pd.Timedelta(days=0.5)
#-- numpy.ndarray of numpy.float64
timebnds_vals = np.append(timebnds_s.to_numpy()[:,None],
                          timebnds_e.to_numpy()[:,None],
                          1)
#-- Specify time_bnds as xarray.DataArray and then merge.
#   Adding time_bnds to ds via 
#   ds['time_bnds'] = xr.DataArray(data=timebnds_vals, dims=("time", "nv"))
#   will cause the time axis encoding to get lost.
time_bnds = xr.DataArray(data=timebnds_vals, dims=("time", "nv"))
ds = xr.merge([ds, time_bnds.rename("time_bnds")])
ds["time"].attrs["bounds"] = "time_bnds"

#-- Set time:cell_methods
ds["tas"].attrs["cell_methods"] = "time: maximum"

#-- Keep xarray from adding _FillValue and coordinates attributes 
#   where it is not supposed to 
#   (see https://github.com/pydata/xarray/issues/5448 & https://github.com/pydata/xarray/issues/5510)
ds["time"].encoding["_FillValue"] = None
ds["lat"].encoding["_FillValue"] = None
ds["lon"].encoding["_FillValue"] = None
ds["time_bnds"].encoding["coordinates"] = None
ds.time_bnds.encoding.update(ds.time.encoding)    #-- time_bnds have int dtype, time has double. Make both double.
ds["tas"].encoding["coordinates"] = 'height lat lon'
#-- In case that tas has no _FillValue attribute, keep xarray from setting it to NAN when writing the netCDF output
#ds["tas"].encoding["_FillValue"] = None

#ds.info()
ds.to_netcdf(f"{odir}/ATMODAT_standardized_xr.nc", format = "NETCDF4_Classic")


# ## Step 6: Now let's look at the ATMODAT standardized netCDF file

# ### (6a) Look at the netCDF file with NCO and CDO

# In[28]:


print("\n", "=" * 10, " ATMODAT standardized netCDF dummy file: the netCDF header   ", "=" * 10)
get_ipython().system('ncdump -v time,time_bnds {odir}/ATMODAT_standardized_xr.nc')

print("\n", "=" * 10, " ATMODAT netCDF dummy file: CDO short info      ", "=" * 10)
get_ipython().system('cdo -s sinfo {odir}/ATMODAT_standardized_xr.nc')


# ### (6b) Evaluate the "pimped" ATMODAT-compliant netCDF files with the atmodat checker
# *Notes* <br>
# * Run the atmodat checker using the command `run_checks` from BASH terminal.
# * Please note that the atmodat checker contains two modules:
#     * one that checks the global attributes for compliance with the ATMODAT standard, 
#     * and another that performs a standard CF check (building upon the cfchecks library).

# **Show usage instructions of the `run_checks`**

# In[29]:


get_ipython().system('run_checks --help')


# **Check a single file: ATMODAT_standardized_xr.nc**

# In[30]:


get_ipython().system('run_checks -s -f {odir}/ATMODAT_standardized_xr.nc')


# **Look at the checker output**<br>
# By default, the output of the last check will be written to atmodat_checker_output/latest/

# In[31]:


print('\n', '=' * 10, ' directory listing of atmodat_checker_output/latest/ ', '=' * 10)
get_ipython().system('ls -lrt  atmodat_checker_output/latest/')

print('\n', '=' * 10, ' content of short_summary.txt ', '=' * 10)
get_ipython().system('cat  atmodat_checker_output/latest/short_summary.txt')

print('\n', '=' * 10, ' content of long_summary_recommended.txt ', '=' * 10)
get_ipython().system('cat  atmodat_checker_output/latest/long_summary_recommended.csv')


# <span style="color:red">The check for the ATMODAT global attributes shows that all mandatory attributes are provided and most of the recommended attribute.<br>
#     The CF checker implemented in the run_checks routine has found no error. <br>
#     Congrats. The data already have a quite high level of FAIRness!  </span>

# **Check the entire directory {odir}**

# In[32]:


get_ipython().system('run_checks -s -p {odir}/')


# **Look at the checker output**<br>

# In[33]:


print('\n', '=' * 10, ' content of short_summary.txt ', '=' * 10)
get_ipython().system('cat  atmodat_checker_output/latest/short_summary.txt')

print('\n', '=' * 10, ' content of long_summary_mandatory.txt ', '=' * 10)
get_ipython().system('cat  atmodat_checker_output/latest/long_summary_mandatory.csv')


# <span style="color:red">The check for the ATMODAT global attributes shows that in the 5 netCDF files checked,
#     only 12 of 19 mandatory attributes are fulfilled.<br>
#     The CF checker implemented in the run_checks routine has found no error, but 9 warnings which should be looked at. <br>
#     For more details, look at the long_summary*.csv files or in the detailed checker report files contained in the subdirectories. <br>
#     </span>
