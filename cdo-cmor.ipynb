{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# cdo cmor: basic standardizing examples (\"CMORizing\")\n",
    "\n",
    "The [CMOR](https://cmor.llnl.gov/) (Climate Model Output Rewriter) software library allows to rewrite\n",
    "climate model output in a form that is compliant with the project requirements. This process is often refered to as \"CMORizing\". \n",
    "\n",
    "CMORizing is necessary for model intercomparison projects (MIPs) so the scientists can benefit from the resulting homogenity in metadata description and data format. Standardized workflows and tools can be applied on the data no matter the source. The alternative would be a big and error prone effort for each individual scientist to process the heterogenous model output of x climate models. The CMORizing process and subsequent quality control is thus an important part of the workflow when submitting data for model intercomparison projects like CMIP6.\n",
    "\n",
    "CMOR has been integrated into the popular [CDOs](https://code.mpimet.mpg.de/) (Climate Data Operators) to facilitate the standardization process and to make it more accessible and appealing to a wider community.\n",
    "\n",
    "To use the cdo cmor operator, cdo has to be compiled including CMOR. \n",
    "This is not done for the common cdo installations on the levante HPC and also not when installing cdo\n",
    "through package managers like APT (eg. `apt-get install cdo`). You can find installation instructions for cdo incl. cmor [here](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_installation).\n",
    "On levante, cdo installations that include CMOR can be found under `/work/bm0021/cdo_incl_cmor/`. The latest version is always accessible via `/work/bm0021/cdo_recent_cmor3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define alias for the desired cdo cmor installation\n",
    "%alias cdo \"/work/bm0021/cdo_incl_cmor/cdo-2022-09-20_cmor3.6.0_gcc/bin/cdo\"\n",
    "%store cdo"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Necessary imports for loading/plotting the standardized data\n",
    "import hvplot.xarray, xarray as xr\n",
    "import matplotlib.pyplot as plt\n",
    "import cartopy.crs as ccrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Project metadata\n",
    "\n",
    "The cdo cmor operator can be partly configured via the command line, however also requires input from configuration files containing project, model and operator specific metadata.\n",
    "\n",
    "To invoke the cdo cmor operator we need to provide several metadata:\n",
    "\n",
    "*  Project metadata / CMOR specific\n",
    "    *  **[Controlled vocabulary](https://github.com/WCRP-CMIP/CMIP6_CVs)** (\"CV\", holding the project metadata including model and experiment definitions as well as part of the projects [DRS (Data Reference Syntax)](https://docs.google.com/document/d/1h0r8RZr_f3-8egBMMh7aqLwy3snpD6_MrDz1q8n5XUk/edit) definition)\n",
    "    *  **[MIP tables](https://github.com/PCMDI/cmip6-cmor-tables/tree/master/Tables)** (metadata related to the variables)\n",
    "* cdo cmor specific\n",
    "  *  **cdocmorinfo** (holding keys that link to the CV [i.e. references to the model or experiment entries of the CV], simulation information and general cdo cmor configuration, can be split over multiple files)\n",
    "  *  **mapping table** (holding the information which model variable corresponds to which variable in the MIP tables)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Controlled vocabulary**\n",
    "\n",
    "The CV can be quite extensive. It also contains a list of required global attributes that are \n",
    "mandatory in every file submitted to the project. Before a file is published via the Earth System Grid Federation ([ESGF](https://esgf.llnl.gov/)), it has to undergo a quality control that performs metadata compliance checks considering both, the CV and the MIP tables. Publication via the DKRZ ESGF node requires additional metadata and data checks. These checks are usually performed by the DKRZ staff."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mip_table_dir = \"/pool/data/CMIP6/cmip6-cmor-tables/Tables/\"\n",
    "CV = mip_table_dir + \"CMIP6_CV.json\"\n",
    "\n",
    "! head -n 44 {CV}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# source-id MPI-ESM1-2-LR\n",
    "!grep -A 2 '\"source_id\":\"MPI-ESM1-2-LR\"' {CV} "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# experiment historical\n",
    "!grep -B 1 '\"experiment_id\":\"historical\"' {CV}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Amon MIP table - tas entry\n",
    "!grep -A 17 '\"tas\":' {mip_table_dir}/CMIP6_Amon.json"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## cdo cmor operator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cdo cmor operator can be called as follows:\n",
    "\n",
    "`cdo cmor,{MIPtable},it={cdocmorinfo_tables_comma_separated},mt={mapping_table},dr={output_path} {input_file}`\n",
    "\n",
    "eg. \n",
    "`cdo cmor,Amon,it=cdocmorinfo,exp.cdocmorinfo,model.cdocmorinfo,mt=test_mapping.txt,dr=./ model_data.nc`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! cdo --help cmor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**cdocmorinfo**\n",
    "\n",
    "Per default, the cdo cmor operator searches for a local and hidden `.cdocmorinfo` file. However, cdocmorinfo files specified in the command line have a higher priority when reading the \n",
    "defined key words."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# cdocmorinfo (also referred to as infotable)\n",
    "it = \"./config/historical_r1i1p1f1-LR.cdocmorinfo\"\n",
    "\n",
    "! cat {it}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Examples\n",
    "\n",
    "In the following 4 examples the CMORization of MPI-ESM1-2-LR model output is demonstrated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input files location"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model_data = \"/work/bm0021/workshopcmip6pp2022/outdata/\"\n",
    "! find {model_data} -type f "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 1: GRIB1 input CMORized to CMIP6 standard\n",
    "\n",
    "The first example showcases the CMORization of the surface temperature variable. \n",
    "The near surface temperature has the GRIB code 167 in the MPI-ESM echam6 output.\n",
    "It also has the variable name tas in the CMIP variable metadata standard, and can be found in various MIP tables. Here we CMORize the CMOR variable tas_Amon.\n",
    "The above information is entered in the mapping table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input file\n",
    "ifile = model_data + \"echam6/historical_r1i1p1f1-LR_echam6_echam_1850.grb\"\n",
    "\n",
    "# Output directory\n",
    "outdir = \"./archive\"\n",
    "\n",
    "# cdocmorinfo - submodel specific\n",
    "it_echam6 = \"./config/cdocmorinfo_echam6_LR\"\n",
    "\n",
    "# mapping table\n",
    "mt = \"./config/mapping_table_example.txt\"\n",
    "\n",
    "# set a fix version date\n",
    "vdate = \"v20221010\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show the content of the mapping table\n",
    "! grep tas {mt}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run cdo cmor**\n",
    "\n",
    "providing the argument `cn=tas` will cause only this variable out of the `Amon` MIP table to be CMORized. If `cn=` is omitted, all variables in `ifile` that are referenced in the mapping table (argument `mt`) will be CMORized and written to the specified output directory (argument `dr`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%cdo cmor,Amon,i={it},{it_echam6},mt={mt},dr={outdir},vd={vdate},cn=tas {ifile}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us take a look ...**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ncdump\n",
    "outputfile = f\"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Amon/\"\n",
    "outputfile += f\"tas/gn/{vdate}/tas_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc\"\n",
    "! ncdump -h {outputfile}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(outputfile)\n",
    "\n",
    "# Plot a single timestep using xarray\n",
    "ds.tas.isel(time=0).plot();\n",
    "\n",
    "# Dynamic plot of the entire timeseries using hvplot\n",
    "#ds.tas.hvplot.quadmesh(width=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 2: GRIB1 input - diagnostic required\n",
    "\n",
    "The next CMOR variable to be created is pr_Amon (precipitation),\n",
    "and requires two model variables as in put - large scale precipitation and convective precipitation.\n",
    "These two variables have the GRIB codes 142 and 143.\n",
    "\n",
    "The operator chaining also works with cdo cmor (with some restrictions that will lead too far for this short introduction), so we can chain an expression to the cdo cmor call:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run cdo cmor**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%cdo cmor,Amon,i={it},{it_echam6},mt={mt},dr={outdir},vd={vdate},cn=pr -expr,pr=var142+var143 {ifile}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us take a look ...**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ncdump\n",
    "outputfile = f\"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Amon/\"\n",
    "outputfile += f\"pr/gn/{vdate}/pr_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc\"\n",
    "! ncdump -h {outputfile}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(outputfile)\n",
    "\n",
    "# Plot a single timestep using xarray\n",
    "ds.pr.isel(time=0).plot();\n",
    "\n",
    "# Dynamic plot of the entire timeseries using hvplot\n",
    "#ds.pr.hvplot.quadmesh(width=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 3: netCDF input\n",
    "\n",
    "This example will create the variable tos out of the MPI-ESM1-2 MPIOM variable of the same name. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input file\n",
    "ifile = model_data + \"mpiom/historical_r1i1p1f1-LR_mpiom_data_2d_mm_18500101_18501231.nc\"\n",
    "\n",
    "# cdocmorinfo - submodel specific attributes\n",
    "it_mpiom = \"./config/cdocmorinfo_mpiom_LR\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us take a look at the source file**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! ncdump -h {ifile} | head -n 40"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run cdo cmor**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%cdo cmor,Omon,i={it},{it_mpiom},mt={mt},dr={outdir},vd={vdate},cn=tos {ifile}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us take a look ...**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ncdump\n",
    "outputfile = f\"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Omon/\"\n",
    "outputfile += f\"tos/gn/{vdate}/tos_Omon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185012.nc\"\n",
    "! ncdump -h {outputfile}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(outputfile)\n",
    "\n",
    "# Plot a single timestep using xarray - the curvilinear ocean grid requires some additional attributes\n",
    "#  for the plot to look proper\n",
    "\n",
    "fig = plt.figure(figsize=[8, 3])\n",
    "ax = plt.subplot(1, 1, 1, projection=ccrs.PlateCarree())\n",
    "ds[\"tos\"].isel(time=0).plot.pcolormesh(ax=ax, x=\"longitude\", y=\"latitude\", \n",
    "                                       shading=\"auto\");\n",
    "\n",
    "# Dynamic plot of the entire timeseries using hvplot\n",
    "#ds[\"tos\"].hvplot.quadmesh(x=\"longitude\", y=\"latitude\", width=600, projection=ccrs.PlateCarree())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example 4: netCDF input - 3D data - diagnostic and gridfile required\n",
    "\n",
    "The next variable (o2sat on ocean layers) requires a gridfile as input with the specified proper layer midpoints interfaces.\n",
    "If no gridfile is provided, cdo cmor will use linear interpolation to infer the layer interfaces\n",
    "from the layer midpoints, which is not correct in this case.\n",
    "The gridfile can be specified in the cdocmorinfo configuration files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input file\n",
    "ifile = model_data + \"mpiom/historical_r1i1p1f1-LR_mpiom_data_3d_mm_18500101_18500131.nc\"\n",
    "\n",
    "# cdocmorinfo - submodel specific attributes\n",
    "it_mpiom = \"./config/cdocmorinfo_mpiom_LR\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us also take a look into the mapping table. Since `o2sat` comes with a vertical dimension, \n",
    "we need to specify a `z_axis` for its mapping table entry."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Show the content of the mapping table\n",
    "! grep Omon {mt}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "cdo cmor does not calculate the vertical bounds correctly, which is why provide a `grid_info`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# A look into the submodel specific configuration\n",
    "! cat {it_mpiom}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This variable requires a quite complex diagnostic, but even this fits into a cdo expression!\n",
    "The underscore prefix allows us to define a temporary variable in a cdo expression that is\n",
    "not written to the resulting output file. Besides arithmetic operations, the expression operator \n",
    "allows the application of a wide selection of functions, like vertical summation, vertical level\n",
    "selection, statistical and conditional operators, etc. \n",
    "(see the [CDO User Guide](https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-3240002.7.1) \n",
    "for more information)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "recipe = \"'_TS=ln((298.15-thetao)/(thetao+273.15));o2sat=0.0446596*\"\n",
    "recipe += \"exp(2.00907+3.22014*_TS+4.05010*_TS^2+4.94457*_TS^3-0.256847*_TS^4+3.88767*_TS^5\"\n",
    "recipe += \"-so*(0.00624523+_TS*0.00737614+0.0103410*_TS^2+0.00817083*_TS^3+0.000000488682*so));'\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Run cdo cmor**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%cdo cmor,Omon,i={it},{it_mpiom},mt={mt},dr={outdir},vd={vdate},cn=o2sat -expr,{recipe} {ifile}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Let us take a look ...**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# ncdump\n",
    "outputfile = f\"{outdir}/CMIP6/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Omon/\"\n",
    "outputfile += f\"o2sat/gn/{vdate}/o2sat_Omon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_185001-185001.nc\"\n",
    "! ncdump -h {outputfile}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = xr.open_dataset(outputfile)\n",
    "\n",
    "# Plot a single timestep using xarray - the curvilinear ocean grid requires some additional attributes\n",
    "#  for the plot to look proper\n",
    "\n",
    "fig = plt.figure(figsize=[8, 3])\n",
    "ax = plt.subplot(1, 1, 1, projection=ccrs.PlateCarree())\n",
    "ds[\"o2sat\"].isel(time=0, lev=0).plot.pcolormesh(ax=ax, x=\"longitude\", y=\"latitude\", \n",
    "                                       shading=\"auto\");\n",
    "\n",
    "# Dynamic plot of the entire timeseries using hvplot\n",
    "#ds[\"o2sat\"].isel(time=0).hvplot.quadmesh(x=\"longitude\", y=\"latitude\", width=600, projection=ccrs.PlateCarree())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Minimal Quality Assurance (QA / QC) using [PrePARE](https://cmor.llnl.gov/mydoc_cmip6_validator/)\n",
    "\n",
    "PrePARE has been created to check the data with regard to the required attributes (CMIP6 CV and DRS).\n",
    "It does not check many aspects of the CF compliance, incomprehensive coordinates etc.\n",
    "For this, for example the [AtMoDat Data Checker](https://github.com/AtMoDat/atmodat_data_checker) or the Tool [QA-DKRZ](https://github.com/IS-ENES-Data/QA-DKRZ) can be used.\n",
    "\n",
    "Please beware, that you should use PrePARE with the same MIP-tables (i.e. MIP-tables of the same `data_specs_version`) that were used to create the standardized files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# PrePARE help message\n",
    "!PrePARE -h"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check all standardized output at once, we need to provide the `table-path` and the output directory we specified previously for the `cdo cmor` execution via the key-word `dr`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!PrePARE --table-path {mip_table_dir} {outdir}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Example of failing PrePARE check**\n",
    "\n",
    "Let's alter the attributes of one of the CMORized files and see PrePARE react:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash -s $outdir\n",
    "\n",
    "tasfile=$( find ${1} -name \"tas_Amon_*.nc\" | head -n 1 )\n",
    "ncatted -O -h -a frequency,global,o,c,\"day\" ${tasfile}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!PrePARE --table-path {mip_table_dir} {outdir}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further information and help\n",
    "\n",
    "*  Mapping tables for CMIP6 and other projects for models supported by DKRZ: [https://c6dreq.dkrz.de](https://c6dreq.dkrz.de) --> Tab `Variable Mapping`\n",
    "*  [CMIP6 DICAD CMORization scripts](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments) for mainly MPI-ESM (access for DKRZ users only)\n",
    "   includes [`cdocmorinfo` files / infotables](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments/-/tree/master/namelist_Scenarios/ssp-HR/scripts/it) used in DICAD.\n",
    "*  [Create CMIP6 `cdocmorinfo` files / infotables](https://c6dreq.dkrz.de/cdocmorinfo/index.html)\n",
    "*  [CMIP6 DICAD CMORizing Tools tutorials](https://c6dreq.dkrz.de/info/workshop_dicad_tools.php)\n",
    "*  [cdo cmor documentation incl. handson](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_Operator)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "DataStandardization_UserWorkshop2022",
   "language": "python",
   "name": "dast_userworkshop22"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
