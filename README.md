# Data Standardization and Data Availability

[![Pages](https://img.shields.io/badge/gitlab-pages-blue)](https://dkrz-user-workshop-dm-training1.gitlab-pages.dkrz.de/data-standardization/intro.html)
[![Gitlab-repo](https://img.shields.io/badge/gitlab-repo-green)](https://gitlab.dkrz.de/dkrz-user-workshop-dm-training1/data-standardization)

The basics and benefits of data standardization including an introduction of tools and services available at DKRZ, incl. practical demonstration in form of jupyter notebooks.

Presentation Slides:
-  [GoogleSlides Data Standardization](https://docs.google.com/presentation/d/1vh4436tW4RzXWCnSuoOPzbz9jErhNTs0) ([pdf-Version](Levante_Einweihung_User_workshop_Data_Standardization.pptx.pdf))
-  [GoogleSlides Data Availability](https://docs.google.com/presentation/d/1H4Ei8staZ7_JthI9ehB4l4dLoymVXul_o4zou5KFU9M)
-  See [GitLab Pages link](https://dkrz-user-workshop-dm-training1.gitlab-pages.dkrz.de/data-standardization/intro.html) for the executed notebooks

Further information and material on the topic of CMORization can be found here:
-  Variable mapping tables for CMIP6 and other projects for models supported by DKRZ: [https://c6dreq.dkrz.de](https://c6dreq.dkrz.de) --> Tab `Variable Mapping`
-  [CMIP6 DICAD CMORization scripts](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments) for mainly MPI-ESM (access for DKRZ users only)
   includes [`cdocmorinfo` files / infotables](https://gitlab.dkrz.de/cmip6-dicad/cmip6_experiments/-/tree/master/namelist_Scenarios/ssp-HR/scripts/it) used in DICAD.
-  [Create CMIP6 `cdocmorinfo` files / infotables](https://c6dreq.dkrz.de/cdocmorinfo/index.html)
-  [CMIP6 DICAD CMORizing Tools tutorials](https://c6dreq.dkrz.de/info/workshop_dicad_tools.php)
-  [cdo cmor documentation incl. handson](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_Operator)

## Use pre-installed conda environment and jupyter kernel

-  Set up the pre-installed jupyter kernel:
   ```
   # Kernel (required for https://jupyterhub.dkrz.de)
   mkdir -p ~/.local/share/jupyter/kernels/
   ln -sf /work/bm0021/conda-envs-public/kernels/share/jupyter/kernels/dast_userworkshop22 ~/.local/share/jupyter/kernels/
   ```
-  Login to levante and download the workshop materials:
   ```
   cd /whereever/you/want
   module load git
   git clone https://gitlab.dkrz.de/dkrz-user-workshop-dm-training1/data-standardization.git
   ```
-  Open https://jupyterhub.dkrz.de in your Browser and spawn a jupyter session ([instructions](https://docs.dkrz.de/doc/software%26services/jupyterhub/index.html)).
-  Navigate to `/whereever/you/want/data-standardization/`, open and execute the jupyter notebooks (eg. `cdo-cmor.ipynb`).

The conda environment can be found here, if required:
```
conda activate /work/bm0021/conda-envs-public/dast_userworkshop22
```

## Alternatively build your own environment and jupyter kernel

Login to levante ... or use terminal in jupyter.

Init conda (example for bash):
```
conda init bash
source ~/.bashrc
```

Get Source:
```
git clone https://gitlab.dkrz.de/dkrz-user-workshop-dm-training1/data-standardization.git
git clone https://github.com/AtMoDat/atmodat_data_checker.git
```

Make conda env:
```
cd data-standardization
mamba env create
conda activate dast_userworkshop22
# or, if conda init failed: source activate dast_userworkshop22
cd ../atmodat_data_checker
pip install -U -e .
git submodule init
git submodule update  --remote --recursive
cd ../data-standardization
```

## Create custom Jupyter kernel for use at https://jupyterhub.dkrz.de

Make kernel:
```
python -m ipykernel install --user --name="dast_userworkshop22" --display-name="DataStandardization_UserWorkshop2022"
```

If necessary, you can set environment variables for your Jupyter kernel using one of the following options:
-  Option 1) Environment variables can be created using `os.environ`:
   ```python
   # Set random environemnt variable:
   import os
   os.environ["CUSTOM_ENV_VAR"] = "custom"
   ```
   Therefore, always add and execute a cell at the top of your jupyter notebook that contains the following code (replace `/path/to/atmodat_data_checker` with your cloned repository path):
   ```
   # Add your conda environment bin dir to PATH (required for example 
   #  if cdo or nco are installed in your custom environment)
   import conda, os
   conda_file_dir = conda.__file__
   conda_dir = conda_file_dir.split('lib')[0]
   proj_lib = os.path.join(os.path.join(conda_dir, 'share'), 'proj')
   udunits_path = os.path.join(os.path.join(os.path.join(conda_dir, 'share'), 'udunits'), 'udunits2.xml')
   os.environ["PROJ_LIB"] = proj_lib
   os.environ["UDUNITS2_XML_PATH"] = udunits_path
   os.environ['PATH'] += os.pathsep + os.path.join(conda_dir, 'bin')
   os.environ['PATH'] += os.pathsep + "/path/to/atmodat_data_checker"
   ```
-  Option 2) Preferably, add the environment variable definition to the kernel.json of your just created jupyter kernel:
   kernel.json per default located at `$HOME/.local/share/jupyter/kernels/dast_userworkshop22/kernel.json`
   Add an `"env"` section like it is done here (the absolute paths however need to be adjusted according to your preferences):
   ```
   {
     "argv":
         ...  
     "env":
     {
         "CONDA_PREFIX":"/work/bm0021/conda-envs-public/dast_userworkshop22",
         "PATH":"${PATH}:/work/bm0021/conda-envs-public/dast_userworkshop22/bin:/work/bm0021/conda-envs-public/repos/atmodat_data_checker",
         "PROJ_LIB":"/work/bm0021/conda-envs-public/dast_userworkshop22/share/proj",
         "UDUNITS2_XML_PATH":"/work/bm0021/conda-envs-public/dast_userworkshop22/share/udunits/udunits2.xml"
     },
     "display_name": "DataStandardization_UserWorkshop2022",
     ...
   }
   ```

Open notebooks in Jupyter and choose kernel "DataStandardization_UserWorkshop2022".


## Add environment variables for the conda environemnt

One can add shell scripts that for example set up environement variables here (default location):
`~/.conda/envs/<ENV_NAME>/etc/conda/activate.d/`
These shell scripts are executed anytime the environment is activated.

Example for a sh script `~/.conda/envs/<ENV_NAME>/etc/conda/activate.d/env_vars.sh`:
```
export PATH=${PATH}:${CONDA_PREFIX}/bin:/work/bm0021/conda-envs-public/repos/atmodat_data_checker
export PROJ_LIB=${CONDA_PREFIX}/share/proj
export UDUNITS2_XML_PATH=${CONDA_PREFIX}/share/udunits/udunits2.xml
```

Note: environment variables defined for the conda environment do not translate to the jupyter kernel and vice versa.


## In case of interest: Freeze conda environment

Update conda spec list on levante:
```
conda list -n dast_userworkshop22 --explicit > spec-list.txt
```

You can use this spec-list.txt to create a new environment:
```
conda env create -f spec-list.txt
```

The spec list of the pre-installed environment on levante can be found in this repository.


## Jupyter Book

### How to create the associated gitlab pages

Build book:
```
jupyter-book build --all .
```

... or use make:
```
make clean build
```

Show build pages:
```
firefox _build/html/index.html
```

### GitHub: Publish book to gh-pages

https://jupyterbook.org/en/stable/publish/gh-pages.html

```
ghp-import -n -p -f _build/html
```

... or use make:
```
make publish
```

### GitLab: Publish book to GitLab pages

Set up a workflow as in `gitlab-ci.yml`. More information in the [GitLab pages documentation](https://gitlab.dkrz.de/help/user/project/pages/index) and the [GitLab automated workflow documentation (external link)](https://docs.gitlab.com/ee/ci/).

___

[Privacy Policy](https://www.dkrz.de/en/about-en/contact/en-datenschutzhinweise) and [Imprint](https://www.dkrz.de/en/about-en/contact/impressum)
